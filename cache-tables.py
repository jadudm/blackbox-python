from __future__ import print_function

import argparse
import inspect
import peewee
import sys

from datetime import datetime, timedelta
from models import *

def date_to_datetime (dstr):
  return datetime.strptime(dstr, '%Y-%m-%d')
  

"""
p_q = (ME.select ()
        .join(P, on = (P.id == ME.project_id))
        .where (P.name == proj_name)
        .where (P.id   == ME.project_id)
        .where (ME.created_at >= sd)
        .where (ME.created_at <= ed))
"""
# get_master_events : date date -> deferred query generator
def get_master_events (start, end, proj_name):
  ME = master_events
  P  = projects

  sd = date_to_datetime(start)
  ed = date_to_datetime(end)
  d1 = timedelta(days = 1)

  day = sd
  mes = set()
  while day != ed:
    start = day
    end   = day + d1
    meq = (ME.select ()
            .where (ME.created_at >= start)
            .where (ME.created_at < end)
            .where (ME.project_id.is_null(False))
            )
    
    
    if meq.exists():
      print("Found {0} master events between {1} and {2}. Filtering.".format(len(meq), start, end))

      count = 1
      for me in meq:
        pq = (P.select()
              .where(P.id   == me.project_id)
              .where(P.name == proj_name)
              )
        if count % 10000 == 0:
          print("Processed {0} MEs.".format(count), end = "\r")
        count += 1

        if pq.exists():
          mes.add(me)

    day = end
    print("Kept {0} mes from {1} to {2}.".format(len(mes), start, end))

  return mes

###
# cache_master_events
# list-of MEs
###
def cache_master_events (mes):
  ME = master_events

  print("Initializing SQLite connection.")
  set_proxy(CACHEDB)
  for me in mes:
    try:
      me.save(force_insert = True)
      print("Saved {0}".format(m.id), end="\r")
    except:
      pass
  print("")

###
# cache_projects
# list-of MEs
###
def cache_projects (mes, proj_name):
  global args
  ME = master_events
  P  = projects

  projs = []
  print("Initializing MySQL connection.")
  set_proxy(QUERYDB)
  
  for me in mes:
    pq = (P.select()   
          .where(P.id == me.project_id)
          .where(projects.name == proj_name))

    if pq.exists():
      projs.extend(list(pq))

  print("Initializing SQLite connection.")
  set_proxy(CACHEDB)

  for p in projs:
    try:
      p.save(force_insert = True)
      print("Saved {0}".format(p.id), end="\r")
    except:
      # Skip anything that already exists.
      pass
  
###
# cache_compile_events
# list-of MEs
###
def cache_compile_events (mes):
  """ 
  This function can work against the local SQLite table
  for its initial queries, because we've just cached them. 
  This *might* speed things up, since there's fewer events 
  in the local table, and SQLite should be fast enough for
  when compared to the MySQL DB.
  """
  ME = master_events
  P  = projects
  CE = compile_events

  # Now, grab all of the associated compilation events.
  set_proxy(QUERYDB)
  ces = set()
  for me in mes:
    ce_q = (CE.select()
            .where(CE.id == me.event_id)
            )
    if ce_q.exists():
      for ce in ce_q:
        ces.add(ce)
  
  # Now, store.
  set_proxy(CACHEDB)
  for ce in ces:
    print("ce: {0}".format(ce.id))
    try:
      ce.save(force_insert = True)
      # print("Saved {0}".format(ce.id), end="\r")
    except:
      # Skip anything that already exists.
      pass
  print("----")

def cache_compile_inputs_and_outputs (mes):
  ME = master_events
  P  = projects
  CE = compile_events
  CI = compile_inputs
  CO = compile_outputs
  
  ces = set()
  set_proxy(CACHEDB)
  q = (CE.select())

  for ce in q:
    ces.add(ce)
  
  cs = set()
  set_proxy(QUERYDB)
  for ce in ces:
    print("ce: {0}".format(ce.id))
        
    q = (CI.select()
          .where(CI.compile_event_id == ce.id)
    )
    print(q.sql())

    if q.exists():
      for c in q:
        print("ce.id: {0} c.compile_event_id: {1}".format(ce.id, c.compile_event_id))
        cs.add(c)

  set_proxy(CACHEDB)
  for c in cs:
    try:
      print("\tce.id: {0} c.compile_event_id: {1}".format(ce.id, c.compile_event_id))
      c.save(force_insert = True)
      print("\tce.id: {0} c.compile_event_id: {1}".format(ce.id, c.compile_event_id))
    except:
      pass

  cs = set()
  set_proxy(QUERYDB)
  for ce in ces:
    q = (CO.select()
          .where(CO.compile_event_id == ce.id)
    )
    if q.exists():
      for c in q:
        cs.add(c)
        
  set_proxy(CACHEDB)
  for c in cs:
    try:
      c.save(force_insert = True)
    except:
      pass


def cache_method_invocation (mes):
  ME = master_events
  MI = invocations

  # Now, grab all of the associated compilation events.
  set_proxy(QUERYDB)
  invo = set()
  for me in mes:
    if me.event_type == "Invocation":
      q = (MI.select()
              .where(MI.id == me.)
              )
      if q.exists():
        for i in q:
          invo.add(i)
    
    # Now, store.
    set_proxy(CACHEDB)
    for i in invo:
      print("invo: {0} - {1}".format(i.id, i.compile_error))
      try:
        i.save(force_insert = True)
      except:
        pass


if __name__ == "__main__":
  
  parser = argparse.ArgumentParser()
  parser.add_argument("--start",    
                      help = "Start date (2014-01-01)")
  parser.add_argument("--end",    
                      help = "End date (2014-01-30")
  parser.add_argument("--proj",   
                      help = "Project name (eg. 'better-ticket-machine'",
                      default = None)
  
  args = parser.parse_args()

  print("Initializing MySQL connection.")
  set_proxy(QUERYDB)

  print("Setting up local cache.")
  initialize_cachedb("cache_{0}_{1}.sqlite".format(args.start, args.end))
  # Should fetch project IDs based on project name, and then
  # use that to get master events. 
  
  print("Getting master events.")
  mes = get_master_events (args.start, args.end, args.proj)

  print("Cache method invocation.")
  cache_method_invocation(mes)
  sys.exit()
  print("Caching master events.")
  cache_master_events (mes)
  print("Caching projects.")
  cache_projects (mes, args.proj)
  print("Grabbing related compilation events.")
  cache_compile_events (mes)
  print("Cache compile inputs and outputs.")
  cache_compile_inputs_and_outputs (mes)
