from configure import Configuration
from peewee import *
import sys

# Load the config file.
config = Configuration.from_file('config.yaml').configure()

# Load the DB password. This is not stored in the repository.
# In fact, it is in the .gitignore, and it needs to stay 
# out of the repository!
f = open(".password", 'r')
db_password = f.readline().strip()
f.close()

#if len(db_password) == 9:
#  print "Password loaded successfully."
#  print "Data: %s" % config.white.database
#  print "Host: %s" % config.white.host
#  print "Port: %s" % config.white.port
#  print "User: %s" % config.white.user


database_proxy = Proxy()

mysql_db = MySQLDatabase (config.white.database,
                          host        = config.white.host,
                          port        = int(config.white.port),
                          user        = config.white.user,
                          password    = db_password,
                          passwd      = db_password
                          )


cache_db = None

def initialize_cachedb (fname):
  global cache_db
  cache_db = SqliteDatabase (fname)
  cache_db.connect()
  try:
    cache_db.create_tables([projects])
    print("Created projects in cache.")
    cache_db.create_tables([master_events])
    print("Created master_events in cache.")
    
    cache_db.close()
  except:
    print("SQLite tables already created.")

QUERYDB = 0
CACHEDB = 1

def set_proxy (the_db):
  global database_proxy, mysql_db, cache_db
  if the_db == QUERYDB:
    try:
      database_proxy.initialize(mysql_db)
      database_proxy.connect()
      print("Connected to the MySQL DB.")
    except:
      pass
  elif the_db == CACHEDB:
    try:
      database_proxy.initialize(cache_db)
      database_proxy.connect()
      cache_db.create_tables([projects])
      cache_db.create_tables([master_events])
      cache_db.create_tables([compile_events])
      cache_db.create_tables([compile_inputs])
      cache_db.create_tables([compile_outputs])
    except:
      pass
  
def close_db ():
  database_proxy.close()

# This class provides us with a base for all Blackbox models.
# Specifically, it specifies the database that PeeWee will 
# look to for all queries.
class Blackbox (Model):
  class Meta:
    database = database_proxy

class projects (Blackbox):
  id                  = BigIntegerField(primary_key = True)
  name                = CharField(255)
  path_hash           = CharField(255, null = True)
  user_id             = BigIntegerField()

# Table: MasterEvents
# As specified in the Blackbox manual. Note that 
# varchars() have been represented as TextFields(), which
# should be fine for querying, but not for insert.
# However, we only have query access, so this should be OK.
class master_events (Blackbox):
  id                  = BigIntegerField(primary_key = True)
  client_address_id   = BigIntegerField()
  created_at          = DateTimeField(null = True)
  event_id            = BigIntegerField(null = True)
  event_type          = CharField(32, null = True)
  name                = CharField(32)
  package_id          = BigIntegerField(null = True)
  participant_id      = BigIntegerField(null = True)
  project_id          = BigIntegerField(null = True)
  sequence_num        = IntegerField()
  session_id          = BigIntegerField()
  source_time         = CharField(32)
  user_id             = BigIntegerField()

class source_hashes (Blackbox):
  id                  = BigIntegerField()
  source_hash         = CharField(64)

class compile_events (Blackbox):
  id                  = BigIntegerField(primary_key = True)
  # Success, in the documentation, is a tinyint.
  # Depending on PeeWee version, this may have to be 
  # an IntegerField() instead of SmallIntegerField()
  success             = IntegerField()
  
class compile_inputs (Blackbox):
  id                  = BigIntegerField(primary_key = True)
  compile_event_id    = BigIntegerField()
  source_file_id      = BigIntegerField()

class compile_outputs (Blackbox):
  id                  = BigIntegerField()
  compile_event_id    = BigIntegerField()
  end_column          = IntegerField()
  end_line            = IntegerField()
  # Should be a SmallIntegerField()
  is_error            = IntegerField()
  message             = CharField(255)
  # Should be a SmallIntegerField()
  shown               = IntegerField()
  source_file_id      = BigIntegerField()
  start_column        = IntegerField()
  start_line          = IntegerField()

class invocations (Blackbox):
  id                  = BigIntegerField()
  bench_object_id     = BigIntegerField()
  code                = TextField()
  compile_error       = CharField(255)
  exception_class     = CharField(255)
  exception_message   = TextField()
  result              = CharField(255)
  session_id          = BigIntegerField()
  session_subid       = IntegerField()
  test_id             = BigIntegerField()
  type_name           = CharField(255)

class debugger_events (Blackbox):
  id                  = BigIntegerField()
  thread_name         = CharField(255)

class breakpoints (Blackbox):
  id                  = BigIntegerField()
  line_number         = IntegerField()
  source_file_id      = BigIntegerField()

class codepad_events (Blackbox):
  id                  = BigIntegerField()
  command             = TextField()
  error               = CharField(255)
  exception           = CharField(255)
  outcome             = CharField(255)
  result              = CharField(255)

class packages (Blackbox):
  id                  = BigIntegerField()
  name                = CharField(255)
  project_id          = BigIntegerField()
 

