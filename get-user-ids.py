import sys
from models import *

# get_user_ids : int int -> generator
# Takes a start and end, and returns a generator that 
# produces a list of ids. Tests whether our PeeWee code 
# works on white.kent.ac.uk.
def get_user_ids (start, end):
  ME = master_events
  q = (ME.select()
          .where(ME.id >= start)
          .where(ME.id <= end)
          )
  return q


if __name__ == "__main__":
  start = 0
  end   = 10
  
  if len(sys.argv) < 2:
    print "Expect a start and end on the command line"
    print "\tpython get-user-ids.py 1 10"
    sys.exit(0)
  else:
    start  = sys.argv[1]
    end    = sys.argv[2]

  # Print the IDs that we found.
  for me in get_user_ids(start, end):
    print("id: {0} user_id: {1}".format(me.id, me.user_id))
