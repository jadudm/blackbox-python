import argparse
import sys
from models import *
from datetime import datetime

HOST = "white"

def date_to_datetime (dstr):
  return datetime.strptime(dstr, '%Y-%m-%d')
  
def get_payload ():
  pass

# Given a start and end project id, this returns the 
# PIDs associated with either a "naive" or "better" 
# ticket machine project.
def get_ticket_machines (start, end, kind):
  P = projects
  ME = master_events
  
#   q = (P.select (P, ME)
#         .join(ME, on = (ME.project_id == P.id))
#         .where (P.name == "{0}-ticket-machine".format(kind))
#         .where (ME.created_at >= date_to_datetime(start))
#         .where (ME.created_at <= date_to_datetime(end))
#         .where (P.id == ME.project_id)
#         )

  me_q = (ME.select ()
            .where (ME.created_at >= date_to_datetime(start))
            .where (ME.created_at <= date_to_datetime(end)))

  id_in_range = []
  for me in me_q:
    # print ("Checking {0}".format(me.id))
    p_q = (P.select()
            .where (P.id == me.project_id)
            .where (P.name == "{0}-ticket-machine".format(kind)))
    if p_q.exists():
      # print("\tAppending {0}".format(me.id))
      id_in_range.append(me)

  return id_in_range
  
  #for p in q:
  #  print("pid: {0}".format(p.id))
  return q

# Given a project id, this returns the events the project contains.
def get_events (pid):
  ME = master_events
  q = (ME.select ()
        .where (ME.project_id == pid))
  return q

if __name__ == "__main__":
  
  parser = argparse.ArgumentParser()
  parser.add_argument("--start",  help = "Start project id.")
  parser.add_argument("--end",    help = "End project id.")
  args = parser.parse_args()

  pids = get_ticket_machines (args.start, args.end, "better")
  for p in pids:
    print ("ME_ID: {0}".format(p.id))
