+----------------------------------------+
| Tables_in_blackbox_production          |
+----------------------------------------+
| Events_Spr2015                         |
| assertions                             |
| bench_objects                          |
| bench_objects_fixtures                 |
| breakpoints                            |
| client_addresses                       |
| codepad_events                         |
| compile_events                         |
| compile_inputs                         |
| compile_outputs                        |
| debugger_events                        |
| extensions                             |
| fixtures                               |
| inspectors                             |
| installation_details                   |
| invocations                            |
| master_events                          |
| packages                               |
| participant_identifiers_for_experiment |
| participants                           |
| projects                               |
| schema_migrations                      |
| sessions                               |
| sessions_for_experiment                |
| sessions_for_experiment_participant    |
| source_files                           |
| source_hashes                          |
| source_histories                       |
| stack_entries                          |
| test_results                           |
| tests                                  |
| users                                  |
| version_control_events                 |
| version_control_files                  |
+----------------------------------------+

