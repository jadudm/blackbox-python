#!/bin/bash

# Source this file:
# source setup.sh

virtualenv -p python2.7 venv
. venv/bin/activate

pip install future
pip install peewee
pip install configure
pip install PyMySQL
